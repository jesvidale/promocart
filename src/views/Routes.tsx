import { BrowserRouter, Routes, Route  } from 'react-router-dom';

import {
  HOME
} from '../constants/paths';

import Cart from './Cart'

const MainRouter = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={HOME} element={<Cart />} />
      </Routes>
    </BrowserRouter>
  )
}

export { MainRouter };
