import styled from 'styled-components';

export const ResumeWrapper = styled.div`
  width: 100%;
  background: white;
  box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.2);
  display: flex;
  flex-direction: column;
  position: relative;
  height: auto;
  transition: height 0.4s;
  &.roll {
    height: 80px;
  }
  &.unroll {
    height: 480px;
  }
  @media ${({ theme }) => theme.mediaquery.laptop} {
    border-radius: 5px;
    width: auto;
    &.roll {
      height: auto;
    }
    &.unroll {
      height: auto;
    }
  }
`

export const Title = styled.span`
  display: block;
  font-size: 15px;
  color: ${({ theme }) => theme.colors.primary};
  margin: 0 14px;
  padding: 10px 0 6px;
  font-weight: bold;
  position: relative;
  @media ${({ theme }) => theme.mediaquery.laptop} {
    margin: 0;
    padding: 14px 18px;
  }
  &::after {
    content: '';
    background-color: ${({ theme }) => theme.colors.primary};
    height: 1px;
    left: 0;
    right: 0;
    bottom: 0;
    position: absolute;
  }
`

export const ResumeSummary = styled.div`
  padding: 8px 14px 10px;
  display: flex;
  flex-direction: column;
  @media ${({ theme }) => theme.mediaquery.laptop} {
    padding: 0 16px;
    display: flex;
    flex-direction: column;
    height: 100%;
  }
`

export const List = styled.ul`
  order: 1;
  height: 400px;
  margin-top: 12px;
  overflow: auto;
  @media ${({ theme }) => theme.mediaquery.laptop} {
    order: 0;
    height: auto;
    margin-top: 0;
    overflow: initial;
  }
`

export const Item = styled.li`
  display: flex;
  flex-direction: row;
  align-items: center;
  flex-wrap: nowrap;
  color: ${({ theme }) => theme.colors.greyFont};
  border-bottom: 1px solid #f6f6f6;
  padding: 20px 0;
  position: relative;
`

export const ItemImg = styled.img`
  width: 56px;
  border: 1px solid ${({ theme }) => theme.colors.greyBorder};
  margin-right: 12px;
`

export const ItemPrice = styled.span`
  font-weight: bold;
  font-size: 16px;
  white-space: nowrap;
  margin-left: auto;
`

export const ItemTitle = styled.span`
  margin-right: 20px;
`

export const SummaryPriceWrapper = styled.div`
  display: flex;
  align-items: baseline;
  margin-top: auto;
  @media ${({ theme }) => theme.mediaquery.laptop} {
    padding: 0 6px 36px;
    margin-top: 24px;
  }
`

export const SummaryPrice = styled.span`
  font-weight: bold;
  margin-left: auto;
  font-size: 17px;
  @media ${({ theme }) => theme.mediaquery.laptop} {
    font-size: 19px;
  }
`

export const SummaryTotal = styled.span`
  font-weight: bold;
  margin-right: 6px;
  font-size: 17px;
  @media ${({ theme }) => theme.mediaquery.laptop} {
    font-size: 19px;
  }
`
