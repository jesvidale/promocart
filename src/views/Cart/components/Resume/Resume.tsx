import { useContext, useState } from "react"
import { OrderContext } from "../../../../context/OrderContext"
import { MI_CESTA, PRODUCTOS, TOTAL } from "../../constants/literals"
import {
  Title,
  ResumeWrapper,
  ResumeSummary,
  List,
  Item,
  ItemImg,
  ItemPrice,
  ItemTitle,
  SummaryPriceWrapper,
  SummaryTotal,
  SummaryPrice
} from "./Resume.style"
import { Roll } from "../../../../components/Roll"
import { IoCloseOutline } from "react-icons/io5"
import { Wrapper } from "../../../../components/Button/Button.style"
import { parsePrice } from "../../utils/price"

const Resume = ():JSX.Element => {
  const { state, dispatch } = useContext(OrderContext)
  const priceList = state.items.map(item => item.price)
  const total = priceList.reduce((a, b) => a + b, 0)
  const [roll, setRoll] = useState(false)
  
  const handleClick = () => {
    setRoll(!roll)
  }
  const removeItem = (id:number) => {
    dispatch({
      type: 'remove',
      id
    })
  }
  return (
    <ResumeWrapper className={roll? 'unroll': 'roll'}>
      <Roll onClickFn={handleClick} />
      <Title>{MI_CESTA}</Title>
      <ResumeSummary>
        { state.total > 0 && 
          <List>
            {state.items.map(({id, title, image, price}, index:number) => (
              <Item key={`${id}${index}`}>
                <Wrapper onClick={() => removeItem(id)} className={'remove'}>
                  <IoCloseOutline />
                </Wrapper>
                <ItemImg src={image}/>
                <ItemTitle>{title}</ItemTitle>
                <ItemPrice>{ parsePrice(price) }</ItemPrice>
              </Item>
            ))}
          </List>
        }
        <SummaryPriceWrapper>
          <SummaryTotal>{TOTAL}</SummaryTotal>
          <span>({state.total} {PRODUCTOS})</span>
          <SummaryPrice>{ parsePrice(total) }</SummaryPrice>
        </SummaryPriceWrapper>
      </ResumeSummary>
    </ResumeWrapper>
  )
}

export { Resume }
