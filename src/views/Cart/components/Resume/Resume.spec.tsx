import { Resume } from ".";
import { ThemeProvider } from "styled-components";
import { theme } from "../../../../assets/styles";
import { render, screen } from "@testing-library/react";
import { OrderContext } from "../../../../context/OrderContext";
import { Summary } from "../../../../interfaces/cart";
import userEvent from "@testing-library/user-event";

const products = [
  {
    id: 1,
    title: 'LaJusticia colágeno con magnesio 450comp',
    price: 14.35,
    image: '/lajusticia-colageno.jpg'
  },
  {
    id: 2,
    title: 'Xhekpon® crema facial 40ml',
    price: 6.49,
    image: '/xhekpon-crema.jpg'
  }
]

const dispatch = jest.fn()

const renderComponent = (mockState:Summary) => {
  const wrapper = (
    <ThemeProvider theme={theme}>
      <OrderContext.Provider value={{state:mockState, dispatch}}>
        <Resume />  
      </OrderContext.Provider>
    </ThemeProvider>
  )
  return render(wrapper)
}

describe('<Resume /> tests', () => {
  it('Should render the component properly', () => {
    const mockState:Summary = {
      total: 0,
      items: []
    }
    renderComponent(mockState)
    const total = screen.getByText(/MI CESTA/i)
    const price = screen.getByText(/0,00 €/i)
    const remove = screen.getByRole('button')
    expect(total).toBeInTheDocument()
    expect(price).toBeInTheDocument()
    expect(remove).not.toHaveClass('remove')
  })
  describe('When cart is not empty', () => {
    beforeEach(() => {
      const mockState:Summary = {
        total: 2,
        items: products
      }
      renderComponent(mockState)
    })
    it('Should render products', () => {
      const totalItems = screen.getByText(/2 productos/i)
      const item = screen.getByText(/LaJusticia colágeno con magnesio 450comp/i)
      const total = document.getElementsByClassName('remove')
      expect(totalItems).toBeInTheDocument()
      expect(item).toBeInTheDocument()
      expect(total.length).toEqual(2)
    })
    it('Should dispatch remove products', () => {
      const items = document.getElementsByClassName('remove')
      userEvent.click(items[0])
      expect(dispatch).toHaveBeenCalled()
    })
  })
})
