import { Catalog } from ".";
import { ThemeProvider } from "styled-components";
import { theme } from "../../../../assets/styles";
import { render } from "@testing-library/react";

const renderComponent = () => {
  const wrapper = (
    <ThemeProvider theme={theme}>
      <Catalog />
    </ThemeProvider>
  )
  return render(wrapper)
}

describe('<Catalog /> tests', () => {
  renderComponent()
  it('renders without crashing', () => {
    expect(true).toBeTruthy()
  })
})

// tests to be implemented
