import { Button } from "../../../../components/Button"
import { Product } from "../../../../interfaces/cart"
import { List, ListItem, ItemTitle, ItemPrice } from './Catalog.style'
import { ANADIR_A_PEDIDO } from "../../constants/literals"
import { useProducts } from "../../hooks/useProducts"
import { OrderContext } from "../../../../context/OrderContext"
import { useContext } from "react"
import { parsePrice } from "../../utils/price"

const Catalog = ():JSX.Element => {
  const products = useProducts()
  const { state, dispatch } = useContext(OrderContext)

  const isAdded = (id:number):boolean => state.items.some(item => item.id === id)

  const addToCart = (item: Product) => {
    dispatch({
      type: 'add',
      item
    })
  }
  return (
    <>
      { products && 
        <List>
          { products.results.map(({id, title, price, image}:Product, index:number) => (
            <ListItem key={`${id}${index}`}>
              <ItemTitle>{ title }</ItemTitle>
              <ItemPrice className={isAdded(id)?'disabled':''}> { parsePrice(price) }</ItemPrice>
              <Button
                className="cart"
                title="Add to cart"
                onClick={() => addToCart({id, title, price, image})}
                disabled={isAdded(id)}
              >
                <span className="sr-only">{ANADIR_A_PEDIDO}</span>
              </Button>
            </ListItem>
          ))}
        </List>
      }
    </>
  )
}

export { Catalog }
