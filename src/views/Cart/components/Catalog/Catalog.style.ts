import styled from 'styled-components';

export const List = styled.ul`
  margin: 0 auto;
`

export const ListItem = styled.li`
  display: flex;
  flex-direction: row;
  align-items: center;
  flex-wrap: nowrap;
  color: ${({ theme }) => theme.colors.greyFont};
  margin-bottom: 22px;
`

export const ItemTitle = styled.span`
  margin-right: 20px;
  flex: 1 1 auto;
`

export const ItemPrice = styled.span`
  padding: 0 10px;
  white-space: nowrap;
  font-weight: bold;
  font-size: 16px;
  &.disabled {
    color: ${({ theme }) => theme.colors.disabled};
  }
  @media ${({ theme }) => theme.mediaquery.laptop} {
    padding: 0 18px;
  }
`
