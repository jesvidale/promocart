import { PageWrapper } from "../../components/PageWrapper"
import { Catalog } from "./components/Catalog"
import { Resume } from "./components/Resume"
import { CatalogWrapper, ResumeWrapper, Wrapper } from "./Cart.style"
import {ErrorBoundary} from 'react-error-boundary'

const Cart = ():JSX.Element => {
  const ErrorFallback = ({error, resetErrorBoundary}:any) => {
    return (
      <div role="alert">
        <p>Something went wrong:</p>
        <pre>{error.message}</pre>
        <button onClick={resetErrorBoundary}>Try again</button>
      </div>
    )
  }
  return (
    <PageWrapper>
      <Wrapper>
        <CatalogWrapper>
          <ErrorBoundary
            FallbackComponent={ErrorFallback}
            onReset={() => {}}
          >
            <Catalog/>
          </ErrorBoundary>
        </CatalogWrapper>
        <ResumeWrapper>
          <Resume/>
        </ResumeWrapper>
      </Wrapper>
    </PageWrapper>
  )
}

export { Cart }
