export const parsePrice = (price:number):string => {
  return `${price.toFixed(2).replace(/\./g,',')} €`
}

