import { parsePrice } from "./price";

describe('Utils', () => {
  it('parsedPrices', () => {
    const prices = [0, 20, 15.75]
    const parsedPrices = ['0,00 €', '20,00 €', '15,75 €']
    let result:string[] = []
    prices.forEach(price => result.push(parsePrice(price)))
    expect(result).toEqual(parsedPrices)
  })
})
