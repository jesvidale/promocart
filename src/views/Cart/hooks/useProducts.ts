import { useEffect, useState } from "react"
import { api } from "../../../api/products"

const useProducts = () => {
  const [products, setProducts] = useState<any>(null)
  useEffect(() => {
    const fetchProducts = async () => {
      const response = await api.requestProducts()
      setProducts(response)
    }
    fetchProducts()
  }, [])
  return products
}

export { useProducts }
