import styled from "styled-components";

export const Wrapper = styled.div`
  margin-top: 50px;
  display: flex;
`


export const CatalogWrapper = styled.section`
  display: flex;
  margin: 0 auto;
  @media ${({ theme }) => theme.mediaquery.laptop} {
    max-width: 580px;
    flex: 1 1 auto;
    padding-right: 80px;
  }
`


export const ResumeWrapper = styled.section`
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  @media ${({ theme }) => theme.mediaquery.laptop} {
    position: static;
    bottom: initial;
    left: initial;
    flex: 1 1 auto;
    width: 400px;
  }
`
