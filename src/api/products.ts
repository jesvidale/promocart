
import { products } from "../mocks/products"

const getProductsFromService = (arg:any) => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(arg)
    }, 0);
  });
}

export const api = {
  requestProducts: async () => {
    let resp
    try {
      resp = await getProductsFromService(products)
    } catch (error:any) {
      console.error('An error occurred while requestCategories request:' + error.message)
    }
    return resp
  }
}
