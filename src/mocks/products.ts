export const products = {
  count: 6,
  results: [
    {
      id: 1,
      title: 'LaJusticia colágeno con magnesio 450comp',
      price: 14.35,
      image: '/lajusticia-colageno.jpg',
    },
    {
      id: 2,
      title: 'Xhekpon® crema facial 40ml',
      price: 6.49,
      image: '/xhekpon-crema.jpg',
    },
    {
      id: 3,
      title: 'Cerave ® Crema Hidratante 340ml ',
      price: 11.70,
      image: '/cerave-crema.jpg',
    },
    {
      id: 4,
      title: 'Hyabak solución 10ml',
      price: 9.48,
      image: '/hyabak-solucion.jpg',
    },
    {
      id: 5,
      title: 'Fotoprotector ISDIN® Fusion Water SPF 50+ 50ml',
      price: 19.74,
      image: '/fotoprotector-isdin.jpg',
    },
    {
      id: 6,
      title: 'Piz Buin® Allergy SPF50+ loción 200ml',
      price: 8.65,
      image: '/piz-buin.jpg',
    },
  ]
}
