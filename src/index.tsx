import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import { ThemeProvider } from 'styled-components'
import { CSSReset, theme } from './assets/styles'


import { MainRouter } from './views/Routes';
import { OrderProvider } from './context/OrderContext'

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <OrderProvider>
        <CSSReset />
        <main id="main">
          <MainRouter/>
        </main>
      </OrderProvider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
