export interface Product {
  id: number,
  title: string,
  price: number,
  image: string | undefined,
}

export interface Summary {
  total: number
  items: Product[]
}

export interface errorBoundary {
  error: any
  resetErrorBoundary: Function
}
