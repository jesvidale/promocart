import { prettyDOM, render, screen } from "@testing-library/react";
import { PageWrapper } from ".";
import { ThemeProvider } from "styled-components";
import { theme } from "../../assets/styles";

describe('<PageWrapper />', () => {
  it('renders without crashing', () => {
    render(
      <ThemeProvider theme={theme}>
        <PageWrapper id='wrapper'>
          <span>Hello world</span>
        </PageWrapper>
      </ThemeProvider>
    )
    const content = screen.getByText(/Hello world/i)
    expect(content).toBeInTheDocument()
  })
})
