import styled from 'styled-components';

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  padding: 0 ${({ theme }) => theme.padding.mobile};
  max-width:  1024px;
  margin: 0 auto;
  position: relative;
  
  @media ${({ theme }) => theme.mediaquery.tablet} {
    padding: 0 ${({ theme }) => theme.padding.tablet};
  }
  
  @media ${({ theme }) => theme.mediaquery.laptop} {
    padding: 0 ${({ theme }) => theme.padding.laptop};
  }
  
  @media ${({ theme }) => theme.mediaquery.desktop} {
    padding: 0 ${({ theme }) => theme.padding.desktop};
  }
`

export { Wrapper }
