import { Wrapper } from './PageWrapper.style'
import { ReactNode } from 'react'

type WrapperProps = {
  children: ReactNode
  className?: string
}

const PageWrapper = (props:WrapperProps) => {
  const {children, className} = props
  return (
    <Wrapper className={className}>{children}</Wrapper>
  )
}

export { PageWrapper }
