import { MouseEventHandler, ReactNode } from 'react';
import { Wrapper } from './Button.style';

type ButtonProps = {
  children?: ReactNode;
  onClick?: MouseEventHandler;
  className?: string
  title?:string
  disabled?:boolean
}

const Button = (props:ButtonProps):JSX.Element => {
  const { children, className, title, onClick, disabled } = props
  return (
    <Wrapper
      className={className}
      title={title}
      onClick={onClick}
      disabled={disabled}
    >{children}</Wrapper>
  )
}

export { Button }
