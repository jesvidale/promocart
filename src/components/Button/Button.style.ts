import styled from 'styled-components';
import CartBg from '../../assets/icons/add-to-cart.svg'
import { darken } from 'polished'

export const Wrapper = styled.button`
  text-decoration: none;
  font-size: 14px;
  border-radius: 5px;
  cursor: pointer;
  display: inline-flex;
  align-items: center;
  justify-content: enter;
  &.cart {
    height: 32px;
    padding: 10px 16px;
    background-color: ${({ theme }) => theme.colors.primary};
    background-image: url(${CartBg});
    background-size: 36px;
    background-position: center;
    background-repeat: no-repeat;
    width: 66px;
    min-width: 66px;
    &:hover {
      background-color: ${props => darken(0.05, props.theme.colors.primary)};
    }
    &:disabled{
      background-color: ${({ theme }) => theme.colors.disabled};
    }
  }
  &.remove {
    height: 20px;
    width: 20px;
    background-color: transparent;
    position: absolute;
    top: 10px;
    right: 0;
    font-size: 20px;
  }
`
