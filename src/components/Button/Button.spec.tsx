import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { ThemeProvider } from "styled-components";
import { theme } from "../../assets/styles";
import { Button } from ".";

describe('<Button/> tests', () => {
  it('Should render the component properly', () => {
    render(
      <ThemeProvider theme={theme}>
        <Button />
      </ThemeProvider>
    )
    const button = screen.getByRole('button')
    userEvent.click(button)
  })
})
