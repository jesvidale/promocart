import styled from "styled-components";

export const Wrapper = styled.button`
  width: 50px;
  height: 25px;
  border-top-left-radius: 25px;
  border-top-right-radius: 25px;
  cursor: pointer;
  background-color: white;
  position: absolute;
  top: -25px;
  left: 0;
  right: 0;
  margin: auto;
  box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.2);
  font-size: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  &.roll {
    >* {
      transform: rotateX(0);
      transition: transform 0.4s;
    }
  }
  &.unroll {
    >* {
      transform: rotateX(180deg);
      transition: transform 0.4s;
    }
  }
  &::after {
    content: '';
    position: absolute;
    left: 0;
    right: 0;
    bottom: -8px;
    width: 50px;
    height: 8px;
    z-index: 4;
    background: white;
  }
  @media ${({ theme }) => theme.mediaquery.laptop} {
    display: none;
  }
`
