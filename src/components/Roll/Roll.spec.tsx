import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { ThemeProvider } from "styled-components";
import { theme } from "../../assets/styles";
import { Roll } from ".";

const handleClick = () => {}

describe('<Roll/> tests', () => {
  beforeAll(() => {
    render(
      <ThemeProvider theme={theme}>
        <Roll onClickFn={handleClick} />
      </ThemeProvider>
    )
  })
  it('Should render the component properly and toggle', () => {
    const roll = screen.getByRole('button')
    expect(roll).toHaveClass('roll')
    userEvent.click(roll)
    expect(roll).toHaveClass('unroll')
  })
})
