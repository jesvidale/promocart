import { ReactNode, useState } from 'react';
import { Wrapper } from './Roll.style';
import { IoChevronUpOutline } from "react-icons/io5";

type RollProps = {
  children?: ReactNode;
  onClickFn: () => void;
  className?: string
  title?:string
}

const Roll = (props:RollProps):JSX.Element => {
  const { title, onClickFn } = props
  const [roll, setRoll] = useState(false)
  const handleClick = () => {
    setRoll(!roll)
    onClickFn()
  }
  return (
    <Wrapper
      className={roll ? 'unroll' : 'roll'}
      title={title}
      onClick={handleClick}
    >
      <IoChevronUpOutline />
    </Wrapper>
  )
}

export { Roll }
