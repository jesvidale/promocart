import { createGlobalStyle } from 'styled-components';

const CSSReset = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    border: 0;
    box-sizing: border-box;
  }

  body {
    font-size: 14px;
    font-family: 'Open Sans', sans-serif;
  }

  p {
    font-size: 16px;
    margin-bottom: 8px;
  }

  ul {
    list-style: none;
  }

  abbr[title] {
    text-decoration: none;
  }

  .sr-only {
    position: absolute;
    width: 1px;
    height: 1px;
    padding: 0;
    margin: -1px;
    overflow: hidden;
    clip: rect(0,0,0,0);
    border: 0;
  }
`;

export { CSSReset }
