import { DefaultTheme } from 'styled-components';

const theme: DefaultTheme = {
  colors: {
    white: '#fff',
    black: '#3d3d3d',
    boxShadow: '#d1d1d1',
    greyBorder: '#d7d7d7',
    greyFont: '#333',
    disabled: '#ccc',
    primary: '#8cbf00',
    error: '#fd615e',
  },
  font: {
    corporate: 'Open Sans, sans-serif',
    corporateSemi: 'Open Sans semibold, sans-serif',
    size: '15px'
  },
  breakpoints: {
    mobile: '320px',
    tablet: '768px',
    laptop: '1024px',
    desktop: '1366px',
    fullhd: '1920px'
  },
  mediaquery: {
    mobile: '(max-width: 767px)',
    tablet: '(min-width: 768px)',
    laptop: '(min-width: 1024px)',
    desktop: '(min-width: 1366px)',
    fullhd: '(min-width: 1920px)'
  },
  padding: {
    mobile: '14px',
    tablet: '20px',
    laptop: '20px',
    desktop: '20px',
    fullhd: '20px'
  }
}

export { theme }
