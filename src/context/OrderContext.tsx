import { createContext, useReducer, ReactNode } from 'react'
import { Product, Summary } from '../interfaces/cart'
import { orderReducer } from './reducers'

type Action = {type: 'add', item: Product} | {type: 'remove', id:number}
export type State = Summary
export type Dispatch = (action: Action) => void
type OrderProviderProps = {children: ReactNode}

const initialState = {
  total: 0,
  items: []
}

const OrderContext = createContext<{
  state: State;
  dispatch: Dispatch}
>({
  state: initialState,
  dispatch: () => null
})

const OrderProvider = ({children}:OrderProviderProps) => {
  const [state, dispatch] = useReducer(orderReducer, initialState)
  return (
    <OrderContext.Provider value={{state, dispatch}}>
      {children}
    </OrderContext.Provider>
  )
}

export { OrderContext, OrderProvider }
