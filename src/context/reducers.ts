import { Product, Summary } from '../interfaces/cart'

type Action = {type: 'add', item: Product} | {type: 'remove', id:number}
type State = Summary

export const orderReducer = (state: State, action: Action) => {
  switch (action.type) {
    case 'add': {
      return { ...state,  total: state.total + 1, items: [ ...state.items, action.item] }
    }
    case 'remove': {
      return { ...state,  total: state.total - 1, items: [...state.items.filter(item => item.id !== action.id)] }
    }
    default: {
      return { ...state }
    }
  }
}
